/*
 *  This version of gulp file only taking care of browserSync reload, js files combine into single file, and production distribution.
 *
 *  1. 'gulp watch' for development
 *  2. 'gulp scripts' to concat all js files into one
 *  3. 'gulp dist' to copy files for distribution
 */
var gulp = require('gulp'),
watch = require('gulp-watch'),
concat = require('gulp-concat'),
browserSync = require('browser-sync').create();

// Change apply for html, css, js file, refresh the browser.
gulp.task('watch', function(){
    browserSync.init({
        server: {
            baseDir: "src"
        }
    });

    watch(['./src/index.html', './src/partials/**/*.scss', './src/styles.scss', './src/app.js'], function(){
        // Reload browser after 3 seconds
        setTimeout(function () {
            browserSync.reload();
        }, 3000);
    });
});

// Run: gulp scripts
gulp.task('scripts', function() {
    var scripts = [
        './src/_js/vue.min.js',
        './src/_js/jquery.min.js',
        './src/_js/app-min.js'
    ];
    gulp.src(scripts)
        .pipe(concat('index.min.js'))
        .pipe(gulp.dest('./src/js/'));
});


// Run gulp dist to copies the files to the /dist folder for distributon
gulp.task('dist', ['scripts'], function() {
    gulp.src('./src/css/*').pipe(gulp.dest('./dist/css'));
    gulp.src('./src/js/*').pipe(gulp.dest('./dist/js'));
    gulp.src('./src/images/*').pipe(gulp.dest('./dist/images'));
    gulp.src('./src/index.html').pipe(gulp.dest('./dist/'));
});