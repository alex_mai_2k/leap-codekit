/*
 *  Main js for the app.
 *
 *  Note: Offcanvas function using vue is having problem, rollback to use simple jQuery
 *
 *  Reference: https://www.npmjs.com/package/vue-offcanvas *
 */

// Below is jQuery code
$( function() {
    $(window).resize(function(){
        if ($(window).width() >= 992 ) {
            $('#main-aside').show();
            $('#main-aside').css({"display": "inline-flex"});
        } else {
            $('#main-aside').hide();
        }
    });

    $('#toggle-mob').click(function () {
        $('#main-aside').animate({width:'toggle'}, 250);

        return false;
    });
});

// Vue components and properity
Vue.component('news', {
    props: ['dateinfo'],
    template: `
            <article class="news-body wrapper">
                <p class="heading"><slot></slot></p>
                <p class="date">{{ dateinfo }}</p>
            </article>
        `
});

Vue.component('task', {
    props: ['title'],
    template: `
            <article class="task-body">
                <div class="icon">icon</div>                
                <div class="task-body-wrapper">                   
                   <div class="description">
                      <p>{{ title }}</p>
                      <p class="message"><slot></slot></p>
                   </div>
                   <div class="more">
                      <div class="label">Label</div>
                    </div>
                </div>                
            </article>
        `
});

var app = new Vue({
    el: '#app',
    data: {
        topMenu: ['Menu', 'Menu'],
        secondMenu: ['Menu', 'Menu', 'Menu'],
        bottomMenu: ['Menu'],
    },
});




